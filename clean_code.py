o = 10
O = 12
I = 20
l = 15

# function


def multiply_by_two(x):
    return x * 2


# variables
user_name = "John"
x = "John Smith"
y, z = x.split()


# classes
class MyClass: ...


# constants

MY_CONSTANT = 10

# module


# packages


class FirstClass:
    def first_method(self): ...

    def second_method(self): ...


class SecondClass: ...


def top_level_function():
    def nested_function(): ...

    def another_nested_function(): ...


def another_top_level_function(
    arg_two,
    arg_three,
    arg_four,
    arg_five,
    arg_six,
    arg_seven,
    arg_eight,
    arg_nine,
    arg_ten,
    arg_one: str = "TTT",
): ...


total = 10 + 20 + 30 + 40
