"""
task_5:
Є рядок, в якому зберігаються 1000 слів. Створіть словник із ключами - унікальними словами та значеннями - кількістю
повторів кожного слова у послідовності.
Виведіть на екран слова, які зустрічаються в тексті більше 10 разів.
"""

from collections import Counter


def get_number_of_words(text: str) -> dict[str, int]:
    """Return dictionary with words and their count in text."""

    #  TODO: add preliminary cleaning for special symbols
    words = set(text.split())
    result_dict = {}
    for word in words:
        result_dict[word] = result_dict.setdefault(word, 0) + 1
    return result_dict


def print_words(words: dict[str, int], num_repeat: int = 10) -> None:
    for word, quantity in words.items():
        if quantity > num_repeat:
            print(f"{word} --> {quantity}")


if __name__ == "__main__":
    # cases = (
    #     ("Hello world", {"Hello": 1, "world": 1}),
    #     ("Hello world world", {"Hello": 1, "world": 2}),
    #     ("Hello world world world", {"Hello": 1, "world": 3}),
    # )
    #
    # for text, expected in cases:
    #     assert (
    #         get_number_of_words(text) == expected
    #     ), f"expected {expected}, but got {get_number_of_words(text)}"

    text = """
    Девід Гантер повертається до Антропологічного дослідницького центру в США, де він колись 
    навчався, відомого як «Трупна ферма». Він сподівається, що ця подорож допоможе йому відточити
    навички судового антрополога та повернути впевненість у собі. Тому погоджується на пропозицію 
    колишнього наставника долучитися до розслідування вбивства. Урешті-решт, Девід буде лише в ролі 
    консультанта — що може піти не так?
    Але навіть Гантер не готовий до такої жорстокості. Зв’язане й піддане тортурам тіло розклалось до 
    невпізнання. Знайдені на місці злочину відбитки пальців вказують на вбивцю, але Девід підозрює, що 
    все не так, як здається на перший погляд.
    А потім знаходять друге тіло, і Гантер розуміє, що серійний убивця володіє вміннями, які йому до 
    болю знайомі, — вміннями криміналіста…
    """

    print_words(
        get_number_of_words(
            text,
        ),
        0,
    )
